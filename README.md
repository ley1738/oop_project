# String statistics - Web Application
Authors: Václav Fabík, Leida Ioani

Semester project from the subject Object-oriented programming.

The goal is a web application, which returns the number of words, sentences, lines, longest and shortest words, longest and shortest sentences from the entered text.

## How to run this amazing app?
1) Open **Git Bash** and go to the folder, where you want to save this project
2) Run the command ```git clone https://gitlab.com/ley1738/oop_project.git```
3) Open the project in **Visual Studio** and click on run the project at **IIS Express (Google Chrome)**
