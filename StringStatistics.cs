﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace Project
{
    public class StringStatistics
    {
        private string text;
        private char[] separator = { ' ', ',', '.', '!', '?', '\n', '(', ')', '\r', '+','-','_','[',']','{','}',';','*','/','|','^','"' }; 
        private string[] words;
        private ArrayList sentences = new ArrayList();

        public String[] LongestSentences { get; private set; }
        public String[] ShortestSentences { get; private set; }
        public String[] LongestWords { get; private set; }
        public String[] ShortestWords { get; private set; }

        public bool IsSentenceNotEmpty { get { if (sentences.Count != 0) return true; else return false; }}

        public StringStatistics(string text)
        {
            this.text = text;
            this.words = text.Split(separator, StringSplitOptions.RemoveEmptyEntries);
        }


        public int countWords()
        {
            return words.Length; //length of separated text
        }

        public int countLines()
        {
            if (text == null || text == "")
                return 0;
            else
                return text.Split('\n').Length; //length of separated text
        }


        public int countSentences()
        {
            int countedSentences = 0;
            int i = 0;
            String text = this.text.Replace("\n", "");
            //goingh through text
            while (i < text.Length)
            {
                //sentence starts w big letter or num
                if (Char.IsUpper(text[i]) || Char.IsDigit(text[i]))
                {
                    String sentence = text[i].ToString();
                    //if we encounter .!? -> end of sentence
                    while (i < text.Length - 1)
                    {
                        i++;
                        sentence += text[i]; //adding char to sentence
                        
                        //can be end of sentence
                        if ((text[i] == '.' || text[i] == '!' || text[i] == '?'))
                        {
                            //end of text
                            if (text.Length == i + 1)
                            {
                                //adding sentence
                                countedSentences++;
                                sentences.Add(sentence);
                                break;
                            }
                            //not end of text
                            else if (text.Length > i + 1)
                            {
                                //whitespace after char + upper letter -> end of sentence  X (decimal separator)
                                if (Char.IsWhiteSpace(text[i + 1]) && (Char.IsUpper(text[i+2]) || Char.IsDigit(text[i + 2])))
                                {
                                    //adding sentence
                                    countedSentences++;
                                    sentences.Add(sentence);
                                    break;
                                }
                            }
                            
                            
                        }
                    }
                }
                i++;
            }
            return countedSentences;
        }

        public int [] sentenceLength()
        {
            Dictionary<String, int> dictionary = new Dictionary<String, int>();
            
            //in countSentences() every sencence was added to arrayList sentences
            foreach (String s in sentences)
            {
                dictionary.Add(s, s.Split(separator, StringSplitOptions.RemoveEmptyEntries).Length);
            }
            int max = dictionary.Values.Max();
            int min = dictionary.Values.Min();
            int[] res = { min, max };

            //string array as property
            ShortestSentences = dictionary.Where(d => d.Value == min).Select(v => v.Key).ToArray();
            LongestSentences = dictionary.Where(d => d.Value == max).Select(v => v.Key).ToArray();
    
            //returning shortest and longest length
            return res;
        }


        public void wordLength()
        {
            int longest = 0;
            int shortest = int.MaxValue;

            foreach (string word in words)
            {
                if (word.Length > longest)
                    longest = word.Length;

                if (word.Length < shortest)
                    shortest = word.Length;
            }

            //properties
            LongestWords = words.Where(word => word.Length == longest).Distinct().ToArray();
            ShortestWords = words.Where(word => word.Length == shortest).Distinct().ToArray();
        }


        public Dictionary<string, int> countFrequencyOfWords()
        {            
            string[] words = this.words; //copy
            Dictionary<string, int> result = new Dictionary<string, int>();

            for (int i =0; i<words.Length; i++)
            {
                var query = from w in words 
                            where w.Equals(words[i], StringComparison.InvariantCultureIgnoreCase)
                            select w;
                result.Add(words[i], query.Count());
                words = words.Where(w => w != words[i]).ToArray();
                i--; //to start again from 0
            }
            return result;
        }
    }
}