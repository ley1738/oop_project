﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Project
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            string tmp = "";
            StringStatistics testString = new StringStatistics(TextBoxInput.Text);

            words.Text = testString.countWords().ToString();
            lines.Text = testString.countLines().ToString();
            sentences.Text = testString.countSentences().ToString();

            testString.wordLength(); //because of properties shortestWords and longestWords

            foreach (string str in testString.ShortestWords) {
                tmp += str.ToString() + ", ";
            }
            if (tmp.Length > 2)
                tmp = tmp.Remove(tmp.Length - 2);
            shortWord.Text = tmp;
            
            tmp = "";
            foreach (string str in testString.LongestWords) {
                tmp += str.ToString() + ", ";
            }
            if (tmp.Length > 2)
                tmp = tmp.Remove(tmp.Length - 2);
            longWord.Text = tmp;


            Dictionary<string, int> wordFrequency = testString.countFrequencyOfWords();
            foreach(KeyValuePair<string, int> entry in wordFrequency)
            {
                TableRow row = new TableRow();
                TableCell cell1 = new TableCell();
                TableCell cell2 = new TableCell();
                cell1.Text = entry.Key;
                cell2.Text = entry.Value.ToString();
                row.Cells.Add(cell1);
                row.Cells.Add(cell2);
                Table2.Rows.Add(row);
            }

            if (testString.IsSentenceNotEmpty)
            {
                int[] len = testString.sentenceLength(); //length
                foreach (string sentence in testString.ShortestSentences)
                {
                    TableRow row = new TableRow();
                    TableCell cell1 = new TableCell();
                    TableCell cell2 = new TableCell();
                    cell1.Text = $"Number of words: {len[0]} ";
                    cell2.Text = sentence;
                    row.Cells.Add(cell1);
                    row.Cells.Add(cell2);
                    Table3.Rows.Add(row);
                }

                foreach (string sentence in testString.LongestSentences)
                {
                    TableRow row = new TableRow();
                    TableCell cell1 = new TableCell();
                    TableCell cell2 = new TableCell();
                    cell1.Text = $"Number of words: {len[1]} ";
                    cell2.Text = sentence;
                    row.Cells.Add(cell1);
                    row.Cells.Add(cell2);
                    Table4.Rows.Add(row);
                }
            }

        }
    }
}