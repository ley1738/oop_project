﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="Project.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="Style.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <div class="headline">
            STRING STATISTICS
        </div>
        <asp:TextBox CssClass="input" ID="TextBoxInput" runat="server" Height="130px" Width="1024" TextMode="MultiLine"></asp:TextBox>
        <p>
            <asp:Button ID="Button1" runat="server" Height="48px" Width="159px" HorizontalAlign="Center" OnClick="Button1_Click" BackColor="Snow" BorderColor="Snow" Text="Analyze text" />
        </p>
        <asp:Table ID="Table1" runat="server" CellPadding="7" GridLines="Both" HorizontalAlign="Center">
            <asp:TableRow CssClass="tbold">
                <asp:TableCell HorizontalAlign="Center"> Words: </asp:TableCell>
                <asp:TableCell HorizontalAlign="Center"> Lines: </asp:TableCell>
                <asp:TableCell HorizontalAlign="Center"> Sentences: </asp:TableCell>
                <asp:TableCell HorizontalAlign="Center"> Shortest word(s): </asp:TableCell>
                <asp:TableCell HorizontalAlign="Center"> Longest word(s): </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell HorizontalAlign="Center" ID="words"> 0 </asp:TableCell>
                <asp:TableCell HorizontalAlign="Center" ID="lines"> 0 </asp:TableCell>
                <asp:TableCell HorizontalAlign="Center" ID="sentences"> 0 </asp:TableCell>
                <asp:TableCell HorizontalAlign="Center" ID="shortWord"> ... </asp:TableCell>
                <asp:TableCell HorizontalAlign="Center" ID="longWord"> ... </asp:TableCell>
            </asp:TableRow>
        </asp:Table>
        <div style="clear:both;"></div>
        <p></p>

        <div style="float: left;margin:0% 3%">
            <asp:Table ID="Table2" CssClass="t2" runat="server" CellPadding="7" GridLines="Both" HorizontalAlign="Left">
                <asp:TableRow>
                    <asp:TableCell CssClass="tbold" ColumnSpan="2" HorizontalAlign="Center"> Words: </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </div>

        <div style="float: right;margin:2% 3%">
            <asp:Table ID="Table3" CssClass="t3" runat="server" CellPadding="7" GridLines="Both" HorizontalAlign="Left">
                <asp:TableRow>
                    <asp:TableCell CssClass="tbold" ColumnSpan="2" HorizontalAlign="Center"> Shortest sentences: </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
            <div style="clear:both;"></div>
            <p></p>
            <asp:Table ID="Table4" CssClass="t4" runat="server" CellPadding="7" GridLines="Both" HorizontalAlign="Left">
                <asp:TableRow>
                    <asp:TableCell CssClass="tbold" ColumnSpan="2" HorizontalAlign="Center"> Longest sentences: </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </div>
        <div style="clear:both;"></div>
        <p></p>
        <footer> © 2022, Václav Fabík, Leida Ioani </footer>
    </form>
    
</body>
</html>
